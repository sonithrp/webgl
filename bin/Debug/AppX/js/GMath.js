GMath = {
    modulo: function (a, b) {
        var r = a % b;
        // If r and b differ in sign, add b to wrap the result to the correct sign.
        return (r * b < 0) ? r + b : r;
    },

    /**
     * Standardizes an angle to be in range [0-360). Negative angles become
     * positive, and values greater than 360 are returned modulo 360.
     * @param {number} angle Angle in degrees.
     * @return {number} Standardized angle.
     */
    standardAngle: function (angle) {
        return GMath.modulo(angle, 360);
    },


    /**
     * Converts degrees to radians.
     * @param {number} angleDegrees Angle in degrees.
     * @return {number} Angle in radians.
     */
    toRadians: function (angleDegrees) {
        return angleDegrees * Math.PI / 180;
    },


    /**
     * Converts radians to degrees.
     * @param {number} angleRadians Angle in radians.
     * @return {number} Angle in degrees.
     */
    toDegrees: function (angleRadians) {
        return angleRadians * 180 / Math.PI;
    },


    /**
     * For a given angle and radius, finds the X portion of the offset.
     * @param {number} degrees Angle in degrees (zero points in +X direction).
     * @param {number} radius Radius.
     * @return {number} The x-distance for the angle and radius.
     */
    angleDx: function (degrees, radius) {
        return radius * Math.cos(GMath.toRadians(degrees));
    },


    /**
     * For a given angle and radius, finds the Y portion of the offset.
     * @param {number} degrees Angle in degrees (zero points in +X direction).
     * @param {number} radius Radius.
     * @return {number} The y-distance for the angle and radius.
     */
    angleDy: function (degrees, radius) {
        return radius * Math.sin(GMath.toRadians(degrees));
    },



    /**
     * Computes the angle between two points (x1,y1) and (x2,y2).
     * Angle zero points in the +X direction, 90 degrees points in the +Y
     * direction (down) and from there we grow clockwise towards 360 degrees.
     * @param {number} x1 x of first point.
     * @param {number} y1 y of first point.
     * @param {number} x2 x of second point.
     * @param {number} y2 y of second point.
     * @return {number} Standardized angle in degrees of the vector from
     *     x1,y1 to x2,y2.
     */
    angle: function (x1, y1, x2, y2) {
        return GMath.standardAngle(GMath.toDegrees(Math.atan2(y2 - y1,
                                                                      x2 - x1)));
    },


    /**
     * Computes the difference between startAngle and endAngle (angles in degrees).
     * @param {number} startAngle  Start angle in degrees.
     * @param {number} endAngle  End angle in degrees.
     * @return {number} The number of degrees that when added to
     *     startAngle will result in endAngle. Positive numbers mean that the
     *     direction is clockwise. Negative numbers indicate a counter-clockwise
     *     direction.
     *     The shortest route (clockwise vs counter-clockwise) between the angles
     *     is used.
     *     When the difference is 180 degrees, the function returns 180 (not -180)
     *     angleDifference(30, 40) is 10, and angleDifference(40, 30) is -10.
     *     angleDifference(350, 10) is 20, and angleDifference(10, 350) is -20.
     */
    angleDifference: function (startAngle, endAngle) {
        var d = GMath.standardAngle(endAngle) -
                GMath.standardAngle(startAngle);
        if (d > 180) {
            d = d - 360;
        } else if (d <= -180) {
            d = 360 + d;
        }
        return d;
    },


    /**
     * Returns the sign of a number as per the "sign" or "signum" function.
     * @param {number} x The number to take the sign of.
     * @return {number} -1 when negative, 1 when positive, 0 when 0.
     */
    sign: function (x) {
        return x == 0 ? 0 : (x < 0 ? -1 : 1);
    }
}
